MDBridge Extension Sample
=========================

This is an example of using the CMF Market Data Bridge (MDBridge) and
the MDBridge Extensions interface in a simple application.

The application uses market data from the CMF Exchange Simulator, which goes
through the MDBridge to a Market Data Depth Manager. This publishes the
market data to a dataview and displays this information on a dashboard.

In this sample, the MDBridge has been created with a custom set of 
extensions for the Market Data Depth datastream type.  In this case, 
the extensions are used to limit the number of levels of the Market 
Data Depth book that it will publish.  The number of levels are 
configurable from an event sent by the controls in the dashboard.

